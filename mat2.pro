#-------------------------------------------------
#
# Project created by QtCreator 2017-01-06T19:14:16
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mat2
TEMPLATE = app

RESOURCES = resources.qrc

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
