#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QSplineSeries>
#include <QMessageBox>
#include <QDebug>
#include <QTextStream>
#include <QtMath>

//http://doc.qt.io/qt-5/qtcharts-callout-example.html

using namespace QtCharts;

MainWindow::MainWindow(QWidget *parent) :
    QGraphicsView(new QGraphicsScene, parent),
    ui(new Ui::MainWindow)
{
    setDragMode(QGraphicsView::NoDrag);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // chart
    QChart *m_chart = new QChart;
    m_chart->setMinimumSize(640, 480);
    m_chart->legend()->hide();

    QScatterSeries *point_series = new QScatterSeries();
    point_series->setMarkerShape(QScatterSeries::MarkerShapeCircle);
    point_series->setMarkerSize(7.0);
    QLineSeries *func_series = new QLineSeries;

    QFile file(":/27_var.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
        exit(EXIT_FAILURE);
    }

    QTextStream in(&file);
    QStringList fields;
    QString line;
    double x, y,
            sum_x = 0,
            sum_y = 0,
            sum_xy = 0,
            sum_x2 = 0,
            sum_real_y = 0;
    int n = 0;
    in.readLine(); // Skip first line.

    while(!in.atEnd()) {
        line = in.readLine().replace(",", ".");
        fields = line.split("\t");
        x = fields.at(0).toDouble();
        y = fields.at(1).toDouble();
        point_series->append(x, y);

        // Ln from negative argument gives complex result
        // with same real part as positive gives. If y are
        // negative, then approx. function also needs to be negated.
        sum_real_y += y;
        y = qLn(fabs(y));

        sum_x += x;
        sum_y += y;
        sum_xy += x * y;
        sum_x2 += qPow(x, 2);
        n++;
    }

    file.close();

    double a1 = (n * sum_xy - sum_x * sum_y) / (n * sum_x2 - qPow(sum_x, 2));
    double a0 = (sum_y - a1 * sum_x) / n;
    double A = ((sum_real_y < 0) ? -1 : 1) * qExp(a0);
    double B = a1;

    m_chart->setTitle(QString().sprintf("A = %f\tB = %f", A, B));

    QPointF p;
    foreach (p, point_series->points()) {
        x = p.x();
        y = A * qExp(B * x);
        func_series->append(x, y);
    }

    m_chart->addSeries(point_series);
    m_chart->addSeries(func_series);

    m_chart->createDefaultAxes();
    /*m_chart->axisY()->setMin(-80);
    m_chart->axisY()->setMax(0);*/

    //m_chart->setAcceptHoverEvents(true);

    setRenderHint(QPainter::Antialiasing);
    scene()->addItem(m_chart);

    /*connect(series, SIGNAL(clicked(QPointF)), this, SLOT(keepCallout()));
    connect(series, SIGNAL(hovered(QPointF, bool)), this, SLOT(tooltip(QPointF,bool)));*/

    //this->setMouseTracking(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}
