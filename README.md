## Install Qt ##
[Download](https://www.qt.io/download-open-source/) and install Qt. Make sure `QtCharts` module is checked for installation.
## Main logic ##
All math and data array filling for visual representation is located in [mainwindow.cpp](https://bitbucket.org/dimandzhi/mat2/src/master/mainwindow.cpp). Current application is done according to tasks 27-th variant.
Compiled result for windows machines is abailable [here](https://bitbucket.org/dimandzhi/mat2/downloads/mat2.zip).
> **Notice**: may not run on Windows ThinPC.
## Deplyment on Windows ##
Use [windeployqt](http://doc.qt.io/qt-5/windows-deployment.html#the-windows-deployment-tool) tool to fetch .dll's. Manually include compiler [dependencies](http://doc.qt.io/qt-5/windows-deployment.html#application-dependencies) if needed.